FROM v2ray/official

RUN apk update;apk add tzdata --no-cache;ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ADD https://github.com/ToutyRater/V2Ray-SiteDAT/releases/download/v0.0.1/h2y.dat /usr/bin/v2ray/h2y.dat

ADD config.json /etc/v2ray/config.json
